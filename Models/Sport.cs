﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DatingIdentity.Models
{
    
    public class Sport
    {
        [Key]
        public int SportId { get; set; }

        [Required]
        [Display(Name = "Sport")]
        public string NomSport { get; set; }

        public ICollection<Pratique> Pratiques{ get; set; }
    }
}
