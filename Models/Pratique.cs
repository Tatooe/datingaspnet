﻿using DatingIdentity.Areas.Identity.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DatingIdentity.Models
{
    public enum Niveau
    {
        débutant,
        confirmé,
        pro,
        supporter
    }
    public class Pratique
    {
        [Key]
        public int PratiqueId { get; set; }

        [Required]
        public Niveau Niveau { get; set; }

        [Display(Name = "Sport")]
        public int SportId { get; set; }
        public Sport Sports { get; set; }

        public string DatingIdentityUserId { get; set; }
        public DatingIdentityUser DatingIdentityUser { get; set; }


    }
}
