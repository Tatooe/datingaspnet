﻿using System;
using DatingIdentity.Areas.Identity.Data;
using DatingIdentity.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

[assembly: HostingStartup(typeof(DatingIdentity.Areas.Identity.IdentityHostingStartup))]
namespace DatingIdentity.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
                services.AddDbContext<DatingIdentityContext>(options =>
                    options.UseSqlServer(
                        context.Configuration.GetConnectionString("DatingIdentityContextConnection")));

                services.AddDefaultIdentity<DatingIdentityUser>(options => options.SignIn.RequireConfirmedAccount = false)
                    .AddRoles<IdentityRole>()
                    .AddEntityFrameworkStores<DatingIdentityContext>()
                    .AddDefaultUI()
                    .AddDefaultTokenProviders();


                services.AddRazorPages();

                services.AddAuthorization(options =>
                {
                    options.FallbackPolicy = new AuthorizationPolicyBuilder()
                        .RequireAuthenticatedUser()
                        .Build();
                });
            });
        }
    }
}