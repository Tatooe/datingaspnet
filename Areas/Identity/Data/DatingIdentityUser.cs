﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using DatingIdentity.Models;
using Microsoft.AspNetCore.Identity;

namespace DatingIdentity.Areas.Identity.Data
{
    // Add profile data for application users by adding properties to the DatingIdentityUser class
    public class DatingIdentityUser : IdentityUser
    {
        [PersonalData]
        [Required(ErrorMessage = "Ce champ est obligatoire")]
        public string Nom { get; set; }

        [PersonalData]
        [Required(ErrorMessage = "Ce champ est obligatoire")]
        [Display(Name = "Prénom")]
        public string Prenom { get; set; }

        [PersonalData]
        [Required(ErrorMessage = "Ce champ est obligatoire")]
        [Display(Name = "Département")]
        public int Departement { get; set; }

        public ICollection<Pratique> Pratiques { get; set; }
        public int? DatingIdentityUserId { get; internal set; }


    }


}
