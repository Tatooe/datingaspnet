﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DatingIdentity.Areas.Identity.Data;
using DatingIdentity.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DatingIdentity.Data
{
    public class DatingIdentityContext : IdentityDbContext<DatingIdentityUser, IdentityRole, string>
    {
        public DatingIdentityContext(DbContextOptions<DatingIdentityContext> options)
            : base(options)
        {
        }
        public DbSet<Pratique> Pratique { get; set; }
        public DbSet<Sport> Sport { get; set; }
        public DbSet<DatingIdentityUser> DatingIdentityUser { get; set; }       

        public DbSet<IdentityRole> Role { get; set; }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);



        }
    }
}
