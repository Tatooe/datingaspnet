﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using DatingIdentity.Areas.Identity.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;

namespace DatingIdentity.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class RegisterModel : PageModel
    {
        private readonly SignInManager<DatingIdentityUser> _signInManager;
        private readonly UserManager<DatingIdentityUser> _userManager;
        private readonly ILogger<RegisterModel> _logger;
        private readonly IEmailSender _emailSender;
        private readonly RoleManager<IdentityRole> _roleManager;

        public RegisterModel(
            UserManager<DatingIdentityUser> userManager,
            SignInManager<DatingIdentityUser> signInManager,
            ILogger<RegisterModel> logger,
            IEmailSender emailSender,
            RoleManager<IdentityRole> roleManager
            )
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _emailSender = emailSender;
            _roleManager = roleManager;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public string ReturnUrl { get; set; }

        public IList<AuthenticationScheme> ExternalLogins { get; set; }

        public class InputModel
        {
            [Required(ErrorMessage = "Le champ {0} est obligatoire")]
            [StringLength(100, ErrorMessage = "Le {0} doit être au minimum de {2} et au maximum de {1} charactères.", MinimumLength = 2)]
            public string Nom { get; set; }
            [Required(ErrorMessage = "Le champ {0} est obligatoire")]
            [StringLength(100, ErrorMessage = "Le {0} doit être au minimum de {2} et au maximum de {1} charactères.", MinimumLength = 2)]
            [Display(Name = "Prénom")]
            public string Prenom { get; set; }
            [Required(ErrorMessage = "Le champ {0} est obligatoire")]
            [Range(1, 95, ErrorMessage ="Merci de saisir un département à 2 chiffres connus en France Métropolitaine.")]
            [Display(Name = "Département")]
            public int Departement { get; set; }

            [Required(ErrorMessage = "Le champ {0} est obligatoire")]
            [EmailAddress(ErrorMessage ="Merci de saisir une adresse mail valide")]
            [Display(Name = "Email")]
            
            public string Email { get; set; }

            [Required(ErrorMessage = "Le champ {0} est obligatoire")]
            [StringLength(100, ErrorMessage = "Le {0} doit être au minimum de {2} et au maximum de {1} charactères.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Mot de Passe")]
            public string Password { get; set; }

            [Required(ErrorMessage = "Le champ {0} est obligatoire")]
            [DataType(DataType.Password)]
            [Display(Name = "Confirmation mot de passe")]
            [Compare("Password", ErrorMessage = "Les mots de passe saisies doivent être identique.")]
            public string ConfirmPassword { get; set; }

            public string Name { get; set; }
        }

        public async Task OnGetAsync(string returnUrl = null)
        {
            //ViewData["roles"] = await _roleManager.Roles.ToListAsync();
            ReturnUrl = returnUrl;
            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl ??= Url.Content("~/");

            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();


            var role = _roleManager.FindByIdAsync(Input.Name).Result;

            if (ModelState.IsValid)
            {
                var user = new DatingIdentityUser { UserName = Input.Email, Email = Input.Email, Nom=Input.Nom, Prenom=Input.Prenom, Departement=Input.Departement };
                var result = await _userManager.CreateAsync(user, Input.Password);

                if (result.Succeeded)
                {
                    _logger.LogInformation("L'utilisateur à créer un nouveau compte avec succés.");
                    //await _userManager.AddToRoleAsync(user, role.Name);

                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(code));
                    var callbackUrl = Url.Page(
                        "/Account/ConfirmEmail",
                        pageHandler: null,
                        values: new { area = "Identity", userId = user.Id, code = code, returnUrl = returnUrl },
                        protocol: Request.Scheme);

                    await _emailSender.SendEmailAsync(Input.Email, "Confirm your email",
                        $"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");

                    if (_userManager.Options.SignIn.RequireConfirmedAccount)
                    {
                        return RedirectToPage("RegisterConfirmation", new { email = Input.Email, returnUrl = returnUrl });
                    }
                    else
                    {
                        await _signInManager.SignInAsync(user, isPersistent: false);
                        return LocalRedirect(returnUrl);
                    }

                    //await _signInManager.SignInAsync(user, isPersistent: false);
                    //return LocalRedirect(returnUrl);
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }

            // If we got this far, something failed, redisplay form
            return Page();
        }
    }
}
