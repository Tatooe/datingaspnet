﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DatingIdentity.ModelViews
{
    public class UserViewModel 
    {
        public UserViewModel()
        {
            Users = new List<string>();
        }
        public string Id { get; set; }

        [Required(ErrorMessage = "Un Rôle est obligatoire")]
        public string RoleName { get; set; }
        public List<string>  Users { get; set; }
    }
}
