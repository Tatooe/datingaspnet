﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DatingIdentity.ViewModels
{
    public class UserRoleViewModel
    {
        public String UserId { get; set; }
        public String RoleId {get; set; }
        public String Email { get; set; }
        public bool IsSelected { get; set; }
    }
}
