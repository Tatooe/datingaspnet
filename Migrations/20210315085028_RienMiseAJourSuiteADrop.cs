﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DatingIdentity.Migrations
{
    public partial class RienMiseAJourSuiteADrop : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Pratique_AspNetRoles_RolesId",
                table: "Pratique");

            migrationBuilder.DropTable(
                name: "DatingIdentityUserRoles");

            migrationBuilder.DropIndex(
                name: "IX_Pratique_RolesId",
                table: "Pratique");

            migrationBuilder.DropColumn(
                name: "RoleUserId",
                table: "Pratique");

            migrationBuilder.DropColumn(
                name: "RolesId",
                table: "Pratique");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "AspNetRoles");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "RoleUserId",
                table: "Pratique",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RolesId",
                table: "Pratique",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "AspNetRoles",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateTable(
                name: "DatingIdentityUserRoles",
                columns: table => new
                {
                    DatingIdentityUsersId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    RolesId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DatingIdentityUserRoles", x => new { x.DatingIdentityUsersId, x.RolesId });
                    table.ForeignKey(
                        name: "FK_DatingIdentityUserRoles_AspNetRoles_RolesId",
                        column: x => x.RolesId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DatingIdentityUserRoles_AspNetUsers_DatingIdentityUsersId",
                        column: x => x.DatingIdentityUsersId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Pratique_RolesId",
                table: "Pratique",
                column: "RolesId");

            migrationBuilder.CreateIndex(
                name: "IX_DatingIdentityUserRoles_RolesId",
                table: "DatingIdentityUserRoles",
                column: "RolesId");

            migrationBuilder.AddForeignKey(
                name: "FK_Pratique_AspNetRoles_RolesId",
                table: "Pratique",
                column: "RolesId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
