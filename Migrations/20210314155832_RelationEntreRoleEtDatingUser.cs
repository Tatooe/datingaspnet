﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DatingIdentity.Migrations
{
    public partial class RelationEntreRoleEtDatingUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DatingIdentityUserRoles",
                columns: table => new
                {
                    DatingIdentityUsersId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    RolesId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DatingIdentityUserRoles", x => new { x.DatingIdentityUsersId, x.RolesId });
                    table.ForeignKey(
                        name: "FK_DatingIdentityUserRoles_AspNetRoles_RolesId",
                        column: x => x.RolesId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DatingIdentityUserRoles_AspNetUsers_DatingIdentityUsersId",
                        column: x => x.DatingIdentityUsersId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DatingIdentityUserRoles_RolesId",
                table: "DatingIdentityUserRoles",
                column: "RolesId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DatingIdentityUserRoles");
        }
    }
}
