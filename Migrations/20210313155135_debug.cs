﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DatingIdentity.Migrations
{
    public partial class debug : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DatingIdentityUserId",
                table: "AspNetUsers",
                type: "int",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DatingIdentityUserId",
                table: "AspNetUsers");
        }
    }
}
