﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DatingIdentity.Migrations
{
    public partial class AjoutSportPratiqueCollectionDansDatingIdentity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Sport",
                columns: table => new
                {
                    SportId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NomSport = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sport", x => x.SportId);
                });

            migrationBuilder.CreateTable(
                name: "Pratique",
                columns: table => new
                {
                    PratiqueId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Niveau = table.Column<int>(type: "int", nullable: false),
                    SportId = table.Column<int>(type: "int", nullable: false),
                    DatingIdentityUserId = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pratique", x => x.PratiqueId);
                    table.ForeignKey(
                        name: "FK_Pratique_AspNetUsers_DatingIdentityUserId",
                        column: x => x.DatingIdentityUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Pratique_Sport_SportId",
                        column: x => x.SportId,
                        principalTable: "Sport",
                        principalColumn: "SportId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Pratique_DatingIdentityUserId",
                table: "Pratique",
                column: "DatingIdentityUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Pratique_SportId",
                table: "Pratique",
                column: "SportId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Pratique");

            migrationBuilder.DropTable(
                name: "Sport");
        }
    }
}
