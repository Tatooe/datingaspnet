﻿using DatingIdentity.Data;
using DatingIdentity.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DatingIdentity.Controllers
{
    [Authorize(Roles = ("User"))]
    public class SportsController : Controller
    {
        private readonly DatingIdentityContext _context;

        public SportsController(DatingIdentityContext context)
        {
            _context = context;
        }

        // GET: SportsController

        public async Task<IActionResult> Index()
        {
            return View(await _context.Sport.ToListAsync());
        }


        // GET: SportsController/Create
        public IActionResult Create()
        {
            return View();
        }


        // POST: SportsController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Sport sport)
        {
                 
                if (ModelState.IsValid)
                {
                    _context.Add(sport);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
         
                return View(sport);
           
        }

        // GET: SportsController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: SportsController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: SportsController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: SportsController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
