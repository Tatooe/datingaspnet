﻿using DatingIdentity.Areas.Identity.Data;
using DatingIdentity.ModelViews;
using DatingIdentity.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DatingIdentity.Controllers
{
    [Authorize(Roles="Admin")]
    public class RoleController : Controller
    {
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<DatingIdentityUser> _userManager;


        public RoleController(RoleManager<IdentityRole> roleManager, UserManager<DatingIdentityUser> userManager)
        {
            _roleManager = roleManager;
            _userManager = userManager;
        }



        // GET: AdminController
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var roles = await _roleManager.Roles.ToListAsync();
            return View(roles);
        }


        // GET: AdminController/Create
        [HttpGet]
        public ActionResult Create()
        {
            return View(new IdentityRole());
        }

        // POST: AdminController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(IdentityRole role)
        {
            if(ModelState.IsValid)
            {
               IdentityResult result = await _roleManager.CreateAsync(role);
           
           
            if(result.Succeeded)
            {
                return RedirectToAction("Index");
            }
            
            foreach(IdentityError error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }
            }
           
            return View(role);
        }

        //private void RoleDropDownList(object selectedRole = null)
        //{
        //    var roleQuery = from d in _roleManager.Roles
        //                    orderby d.Name
        //                    select d;

        //    ViewBag.Id = new SelectList(roleQuery.AsNoTracking(), "Id", "Name", selectedRole);

        //}

        [HttpGet]
        public async Task<ActionResult> EditRole(string Id)
        {
            if (Id == null)
            {
                return NotFound();
            }

            var roleExist = await _roleManager.FindByIdAsync(Id);

            if (ModelState.IsValid)
            {

                if (roleExist == null)
                {
                    ViewBag.ErrorMessage = $"Le rôle avec l'ID {Id} n'a pas été trouvé.";
                    return View("NotFound");
                }

                var model = new UserViewModel
                {
                    Id = roleExist.Id,
                    RoleName = roleExist.Name
                };

                foreach (var user in _userManager.Users)
                {
                    if (await _userManager.IsInRoleAsync(user, roleExist.Name))
                    {
                        model.Users.Add(user.UserName);
                    }
                }

                return View(model);
            }
            //RoleDropDownList(Id);
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> EditRole(UserViewModel model)
        {
 
            var roleExist = await _roleManager.FindByIdAsync(model.Id);

            if (ModelState.IsValid)
            {

                if (roleExist == null)
                {
                    ViewBag.ErrorMessage = $"Le rôle avec l'ID {model.Id} n'a pas été trouvé.";
                    return View("NotFound");
                }
                else
                {
                    roleExist.Name = model.RoleName;
                   var result = await _roleManager.UpdateAsync(roleExist);

                    if(result.Succeeded)
                    {
                        return RedirectToAction("Index");
                    }

                    foreach(var error in result.Errors)
                    {
                        ModelState.AddModelError("", error.Description);
                    }
                    return View(model);
                } 
                
            }
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> EditUsersInRole(string roleId)
        {
            ViewBag.roleId = roleId;

            var role = await _roleManager.FindByIdAsync(roleId);

            if (role == null)
            {
                ViewBag.ErrorMessage = $"Le rôle avec l'Id {roleId} n'a pas été trouvé";
                return View("NotFound");
            }
            var model = new List<UserRoleViewModel>();

            foreach(var user in _userManager.Users)
            {
                var userRoleViewModel = new UserRoleViewModel
                {
                    UserId = user.Id,
                    Email = user.Email
                };

                if(await _userManager.IsInRoleAsync(user, role.Name))
                {
                    userRoleViewModel.IsSelected = true;
                }
                else
                {
                    userRoleViewModel.IsSelected = false;
                }

                model.Add(userRoleViewModel);
            }
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> EditUsersInRole(List<UserRoleViewModel> model, string roleId)
        {
         

            var role = await _roleManager.FindByIdAsync(roleId);

            if (role == null)
            {
                ViewBag.ErrorMessage = $"Le rôle avec l'id {roleId} n'a pas été trouvé";
                return View("NotFound");
            }

            for(int i = 0; i < model.Count; i++)
            {
              var user =  await _userManager.FindByIdAsync(model[i].UserId);

                IdentityResult result = null;

                if (model[i].IsSelected && !(await _userManager.IsInRoleAsync(user, role.Name)))
                {
                    result = await _userManager.AddToRoleAsync(user, role.Name);

                }
                else if (!model[i].IsSelected && await _userManager.IsInRoleAsync(user, role.Name))
                {
                    result = await _userManager.RemoveFromRoleAsync(user, role.Name);
                }
                else
                {
                    continue;
                }

                if(result.Succeeded)
                {
                    if (i < (model.Count - 1))
                    {
                        continue;
                    }else{
                        return RedirectToAction("EditRole", new { Id = roleId });
                    }
                }
            }
                        
           return RedirectToAction("EditRole", new { Id = roleId });
        }



    }
}
