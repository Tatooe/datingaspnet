﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DatingIdentity.Data;
using DatingIdentity.Models;
using Microsoft.AspNetCore.Identity;
using DatingIdentity.Areas.Identity.Data;
using Microsoft.AspNetCore.Authorization;

namespace DatingIdentity.Controllers
{
    public class PratiquesController : Controller
    {
        private readonly DatingIdentityContext _context;
        
        

        public PratiquesController(DatingIdentityContext context)
        {
            _context = context;
        }


        // GET: Pratiques
        [Authorize(Roles = "User")]
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Pratique.Include(p => p.DatingIdentityUser).Include(p => p.Sports).AsNoTracking(); 
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Pratiques/Details/5
        [Authorize(Roles = "User")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var pratique = await _context.Pratique
                .Include(p => p.DatingIdentityUser)
                .Include(p => p.Sports)
                .FirstOrDefaultAsync(m => m.PratiqueId == id);
            if (pratique == null)
            {
                return NotFound();
            }

            return View(pratique);
        }

        // GET: Pratiques/Create
        [Authorize(Roles = "User")]
        public IActionResult Create()
        {
                    
            SportDropdownList();
            return View();
        }

        // POST: Pratiques/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "User")]
        public async Task<IActionResult> Create([Bind("PratiqueId,Niveau,SportId,DatingIdentityUserId")] Pratique pratique)
        {
            if (ModelState.IsValid)
            {
                _context.Add(pratique);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            SportDropdownList(pratique.SportId);
            return View(pratique);
        }

        private void SportDropdownList(object selectedSport = null)
        {
            var sportQuery = from d in _context.Sport
                             orderby d.NomSport
                             select d;
            ViewBag.SportId = new SelectList(sportQuery.AsNoTracking(), "SportId", "NomSport", selectedSport);
        }

        // GET: Pratiques/Edit/5
        [Authorize(Roles = "User")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var pratique = await _context.Pratique
                .Include(i => i.Sports)                
                .AsNoTracking()
                .FirstOrDefaultAsync(m => m.PratiqueId == id);


                
            if (pratique == null)
            {
                return NotFound();
            }
            SportDropdownList();
            return View(pratique);
        }

        // POST: Pratiques/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "User")]
        public async Task<IActionResult> Edit(int id, [Bind("PratiqueId,Niveau,SportId,DatingIdentityUserId")] Pratique pratique)
        {
            if (id != pratique.PratiqueId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(pratique);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PratiqueExists(pratique.PratiqueId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            SportDropdownList(pratique.SportId);
            return View(pratique);
        }

        // GET: Pratiques/Delete/5
        [Authorize(Roles = "User")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var pratique = await _context.Pratique
                .Include(p => p.DatingIdentityUser)
                .Include(p => p.Sports)
                .FirstOrDefaultAsync(m => m.PratiqueId == id);
            if (pratique == null)
            {
                return NotFound();
            }

            return View(pratique);
        }

        // POST: Pratiques/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "User")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var pratique = await _context.Pratique.FindAsync(id);
            _context.Pratique.Remove(pratique);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PratiqueExists(int id)
        {
            return _context.Pratique.Any(e => e.PratiqueId == id);
        }
    }
}
